import redis
import json
import time
from datetime import datetime
from random import random
from loguru import logger


TICKER_TEMPLATE = "ticker_{:02d}"
NUM_TICKERS = 100


def generate_movement() -> int:
    movement = -1 if random() < 0.5 else 1
    return movement


def startup() -> None:
    logger.info("Starting UP. Writing initial data.")
    for num in range(NUM_TICKERS):
        ticker_name = TICKER_TEMPLATE.format(num)
        current_ts = int(datetime.utcnow().timestamp() * 1000)
        data = data = [current_ts, 0, 20, -20, 50, 97]
        redis_client.lpush(ticker_name, json.dumps(data))
    logger.info("Initial data written.")
    logger.info("Service successfully started!")


def generate() -> None:
    logger.info("Start generating data")
    tickers_values = {TICKER_TEMPLATE.format(num): 0 for num in range(NUM_TICKERS)}
    while True:
        for name, value in tickers_values.items():
            movement = generate_movement()
            current_ts = int(datetime.utcnow().timestamp() * 1000)
            data = [current_ts, value + movement, value + 20, value - 20, 50, 97]
            redis_client.lpush(name, json.dumps(data))
            tickers_values[name] += movement
        time.sleep(1)


if __name__ == "__main__":
    logger.info("Connecting redis")
    redis_client = redis.StrictRedis(host="redis")
    logger.info("Redis successfully connected")
    startup()
    generate()

