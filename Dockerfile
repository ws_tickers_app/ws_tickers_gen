# Alpine version wasn't chosen due to simplification of poetry install
FROM python:3.8 
RUN pip install poetry

# Copying only requirements to cache them in docker layer
WORKDIR /code
COPY poetry.lock pyproject.toml /code/

# Project initialization:
RUN poetry install --no-interaction --no-ansi

# Creating folders, and files for a project:
COPY . /code
CMD ["poetry", "run", "python", "main.py"]